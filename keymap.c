#include QMK_KEYBOARD_H

/* Specify layers to be used */
enum zero_sixty_layers {
  _COLEMAKDH,
  _QWERTY,
  _MEDIA,
  _NAVIGATION,
  _MOUSE,
  _SYMBOLS,
  _NUMBERS,
  _FUNCTIONS,
  _BUTTON,
};

/* Aliases */
/* Base layers */
#define _BASE _COLEMAKDH
#define _ALT _QWERTY
/* additional layers */
/* on the right, activated by left thumb cluster */
#define ES_MEDI LT(_MEDIA, KC_ESC)
#define SP_NAVI LT(_NAVIGATION, KC_SPC)
#define TA_MOUS LT(_MOUSE, KC_TAB)
/* on the left, activated by right thumb cluster */
#define EN_SYMB LT(_SYMBOLS, KC_ENT)
#define BS_NUMB LT(_NUMBERS, KC_BSPC)
#define DE_FUNC LT(_FUNCTIONS, KC_DEL)
/* mirrored, pinky */
#define MO_BTTN MO(_BUTTON)
/* tap / hold */
/* +++ colemak dh +++ */
/* left half */
#define _LGUI_A LGUI_T(KC_A)
#define _LALT_R LALT_T(KC_R)
#define _LCTL_S LCTL_T(KC_S)
#define _LSFT_T LSFT_T(KC_T)
#define _LTBU_Z LT(_BUTTON, KC_Z)
#define _ALGR_X ALGR_T(KC_X)
/* right half */
#define _LGUI_O LGUI_T(KC_O)
#define _LALT_I LALT_T(KC_I)
#define _LCTL_E LCTL_T(KC_E)
#define _LSFT_N LSFT_T(KC_N)
#define _LTBU_S LT(_BUTTON, KC_SLSH)
#define _ALGR_D ALGR_T(KC_DOT)
/* --- colemak dh --- */
/* +++ qwerty +++ */
/* left half */
#define _LGUI_A LGUI_T(KC_A)
#define _LALT_S LALT_T(KC_S)
#define _LCTL_D LCTL_T(KC_D)
#define _LSFT_F LSFT_T(KC_F)
#define _LTBU_Z LT(_BUTTON, KC_Z)
#define _ALGR_X ALGR_T(KC_X)
/* right half */
#define _LGUI_S LGUI_T(KC_SCLN)
#define _LALT_L LALT_T(KC_L)
#define _LCTL_K LCTL_T(KC_K)
#define _LSFT_J LSFT_T(KC_J)
#define _LTBU_S LT(_BUTTON, KC_SLSH)
#define _ALGR_D ALGR_T(KC_DOT)
/* --- qwerty --- */
/* +++ windows specific +++ */
#define KC_REDO C(KC_Y)
#define KC_PASTE C(KC_V)
#define KC_COPY C(KC_C)
#define KC_CUT C(KC_X)
#define KC_UNDO C(KC_Z)
/* --- windows specific --- */
/* +++ switch between keymaps +++ */
#define C_QWERT DF(_QWERTY)
#define C_CMKDH DF(_COLEMAKDH)
/* --- switch between keymaps --- */

/* Define the specified layers */
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/*
  [_EMPTY] = LAYOUT_1x2uC(
    _______,  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
    _______,  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
    _______,  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
    _______,  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
    _______,  _______, _______, _______, _______,     _______     , _______, _______, _______, _______, _______
  ),
  */

[_QWERTY] = LAYOUT_1x2uC (
  _______, _______, _______, _______, _______,  _______, _______, _______,  _______, _______, _______, _______,
  KC_Q   , KC_W   , KC_E   , KC_R   , KC_T   ,  _______, _______, KC_Y   ,  KC_U   , KC_I   , KC_O   , KC_P   ,
  _LGUI_A, _LALT_S, _LCTL_D, _LSFT_F, KC_G   ,  _______, _______, KC_H   ,  _LSFT_J, _LCTL_K, _LALT_L, _LGUI_S,
  _LTBU_Z, _ALGR_X, KC_C   , KC_V   , KC_B   ,  _______, _______, KC_N   ,  KC_M   , KC_COMM, _ALGR_D, _LTBU_S,
  _______, _______, ES_MEDI, SP_NAVI, TA_MOUS,      _______     , EN_SYMB,  BS_NUMB, DE_FUNC, _______, _______
),

[_COLEMAKDH] = LAYOUT_1x2uC (
  _______, _______, _______, _______, _______,  _______, _______, _______,  _______, _______, _______, _______,
  KC_Q   , KC_W   , KC_F   , KC_P   , KC_B   ,  _______, _______, KC_J   ,  KC_L   , KC_U   , KC_Y   , KC_QUOT,
  _LGUI_A, _LALT_R, _LCTL_S, _LSFT_T, KC_G   ,  _______, _______, KC_M   ,  _LSFT_N, _LCTL_E, _LALT_I, _LGUI_O,
  _LTBU_Z, _ALGR_X, KC_C   , KC_D   , KC_V   ,  _______, _______, KC_K   ,  KC_H   , KC_COMM, _ALGR_D, _LTBU_S,
  _______, _______, ES_MEDI, SP_NAVI, TA_MOUS,      _______     , EN_SYMB,  BS_NUMB, DE_FUNC, _______, _______
),

/* +++ layers on the right, activated with the left thumb cluster +++ */

[_NAVIGATION] = LAYOUT_1x2uC (
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
  RESET  , C_QWERT, C_CMKDH, _______, _______, _______, _______, KC_REDO, KC_PSTE, KC_COPY, KC_CUT , KC_UNDO,
  KC_LGUI, KC_LALT, KC_LCTL, KC_LSFT, _______, _______, _______, KC_CAPS, KC_LEFT, KC_DOWN, KC_UP  , KC_RGHT,
  _______, KC_ALGR, _______, _______, _______, _______, _______, KC_INS , KC_HOME, KC_PGUP, KC_PGDN, KC_END ,
  _______, _______, _______, _______, _______,      _______    , KC_ENT , KC_BSPC, KC_DEL , _______, _______
),

[_MOUSE] = LAYOUT_1x2uC (
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
  RESET  , _______, _______, _______, _______, _______, _______, KC_REDO, KC_PSTE, KC_COPY, KC_CUT , KC_UNDO,
  KC_LGUI, KC_LALT, KC_LCTL, KC_LSFT, _______, _______, _______, _______, KC_MS_L, KC_MS_D, KC_MS_U, KC_MS_R,
  _______, KC_ALGR, _______, _______, _______, _______, _______, _______, KC_WH_L, KC_WH_D, KC_WH_U, KC_WH_R,
  _______, _______, _______, _______, _______,      _______    , KC_BTN1, KC_BTN3, KC_BTN2, _______, _______
),

[_MEDIA] = LAYOUT_1x2uC (
  _______, _______, _______, _______, _______,  _______, _______, _______, _______, _______, _______, _______,
  RESET  , _______, _______, _______, _______,  _______, _______, RGB_TOG, RGB_MOD, RGB_HUI, RGB_SAI, RGB_VAI,
  KC_LGUI, KC_LALT, KC_LCTL, KC_LSFT, _______,  _______, _______, _______, KC_MPRV, KC_VOLD, KC_VOLU, KC_MNXT,
  _______, KC_ALGR, _______, _______, _______,  _______, _______, _______, _______, _______, _______, _______,
  _______, _______, _______, _______, _______,      _______     , KC_MSTP, KC_MPLY, KC_MUTE, _______, _______
),

/* --- layers on the right, activated with the left thumb cluster --- */

/* +++ layers on the left, activated with the right thumb cluster +++ */

[_NUMBERS] = LAYOUT_1x2uC (
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
  KC_LBRC, KC_7   , KC_8   , KC_9   , KC_RBRC, _______, _______, _______, _______, _______, _______, RESET  ,
  KC_SCLN, KC_4   , KC_5   , KC_6   , KC_EQL , _______, _______, _______, KC_LSFT, KC_LCTL, KC_LALT, KC_LGUI,
  KC_GRV , KC_1   , KC_2   , KC_3   , KC_BSLS, _______, _______, _______, _______, _______, KC_ALGR, _______,
  _______, _______, KC_DOT , KC_0   , KC_MINS,     _______     , _______, _______, _______, _______, _______
),

[_SYMBOLS] = LAYOUT_1x2uC (
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
  KC_LCBR, KC_AMPR, KC_ASTR, KC_LPRN, KC_RCBR, _______, _______, _______, _______, _______, _______, RESET  ,
  KC_COLN, KC_DLR,  KC_PERC, KC_CIRC, KC_PLUS, _______, _______, _______, KC_LSFT, KC_LCTL, KC_LALT, KC_LGUI,
  KC_TILD, KC_EXLM, KC_AT,   KC_HASH, KC_PIPE, _______, _______, _______, _______, _______, KC_ALGR, _______,
  _______, _______, KC_LPRN, KC_RPRN, KC_UNDS,     _______     , _______, _______, _______, _______, _______
),

[_FUNCTIONS] = LAYOUT_1x2uC (
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
  KC_F12 ,  KC_F7 , KC_F8  , KC_F9  , KC_PSCR, _______, _______, _______, _______, _______, _______, RESET  ,
  KC_F11 ,  KC_F4 , KC_F5  , KC_F6  , KC_SLCK, _______, _______, _______, KC_LSFT, KC_LCTL, KC_LALT, KC_LGUI,
  KC_F10 ,  KC_F1 , KC_F2  , KC_F3  , KC_PAUS, _______, _______, _______, _______, _______, KC_ALGR, _______,
  _______, _______, KC_APP , KC_SPC , KC_TAB ,     _______     , _______, _______, _______, _______, _______
),

/* --- layers on the left, activated with the right thumb cluster --- */

/* +++ mirrored, pinky +++ */

[_BUTTON] = LAYOUT_1x2uC (
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
  KC_UNDO, KC_CUT , KC_COPY, KC_PSTE, KC_REDO, _______, _______, KC_REDO, KC_PSTE, KC_COPY, KC_CUT , KC_UNDO,
  KC_LGUI, KC_LALT, KC_LCTL, KC_LSFT, _______, _______, _______, _______, KC_LSFT, KC_LCTL, KC_LALT, KC_LGUI,
  KC_UNDO, KC_CUT , KC_COPY, KC_PSTE, KC_REDO, _______, _______, KC_REDO, KC_PSTE, KC_COPY, KC_CUT , KC_UNDO,
  _______, _______, KC_BTN2, KC_BTN3, KC_BTN1,      _______    , KC_BTN1, KC_BTN3, KC_BTN2, _______, _______
),

/* --- mirrored, pinky --- */

};
